## Set up Google API

1. https://console.cloud.google.com/apis/credentials
2. Set up API Key, OAuth consent screen and OAuth 2.0 Client ID


**OAuth consent screen**

- App name
- User support email
- Developer email address
- Scope: `Google Sheets API	.../auth/spreadsheets`
- Test users: your developers


**OAauth 2.0 Client ID:**

- Web application
- Name
- Authorized redirect URI: `http://localhost`

## Set up your Google sheet

|Key|Value|Comment|
|---|---|---|
|MAILGUN_KEY|XY|Comment|

## Set up .env

```shell
ENV_DOWNLOADER_GOOGLE_KEY=XY
ENV_DOWNLOADER_GOOGLE_CLIENT_ID=XY
ENV_DOWNLOADER_GOOGLE_SECRET=XY
ENV_DOWNLOADER_GOOGLE_TOKEN_PATH=gtoken.json
ENV_DOWNLOADER_SPREADSHEET_ID=XY
ENV_DOWNLOADER_SHEET_NAME='List 1'
ENV_DOWNLOADER_OUTPUT_PATH='.env.dev'
```

## Usage

```shell
npm i -g @mattproch-dev/env-downloader

env-download
```

**Download**

- Downloads all the envs into the specified env file
