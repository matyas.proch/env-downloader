import * as dotenv from 'dotenv';
import * as express from 'express';
import * as fs from 'fs';
import { google } from 'googleapis';

dotenv.config();

const config = {
  port: 4444,
  googleKey: process.env.ENV_DOWNLOADER_GOOGLE_KEY,
  googleClientId: process.env.ENV_DOWNLOADER_GOOGLE_CLIENT_ID,
  googleSecret: process.env.ENV_DOWNLOADER_GOOGLE_SECRET,
  googleToken: process.env.ENV_DOWNLOADER_GOOGLE_TOKEN_PATH,
  googleScopes: ['https://www.googleapis.com/auth/spreadsheets'],
  googleSpreadsheetId: process.env.ENV_DOWNLOADER_SPREADSHEET_ID,
  googleSheetName: process.env.ENV_DOWNLOADER_SHEET_NAME,
  outputEnv: process.env.ENV_DOWNLOADER_OUTPUT_PATH,
};

const loggedIn = (auth) => {
  const sheets = google.sheets({ version: 'v4', auth });

  type Data = string[][];

  const download = (data: Data) => {
    if (data.length === 0) {
      return;
    }

    const output = [];

    data.slice(1).forEach(row => {
      output.push(`${row[0]}='${row[1]}'`)
    });

      if (fs.existsSync(config.outputEnv)) {
        fs.unlinkSync(config.outputEnv);
      }

      fs.writeFileSync(
        config.outputEnv,
        output.join('\n'),
      );

    console.log('Downloaded');
  };

  sheets.spreadsheets.values.get(
    {
      key: config.googleKey,
      spreadsheetId: config.googleSpreadsheetId,
      range: config.googleSheetName,
    },
    (err, res) => {
      if (err) {
        return console.log(`The API returned an error: ${err}`);
      }

      const data = res.data.values ?? [];

      download(data);
    },
  );
};

function getNewToken(oAuth2Client, callback) {
  const app = express();

  const server = app.listen(config.port, () => {});

  app.get('/', (req, res) => {
    const { code } = req.query;

    if (code) {
      res.status(200).send(code);

      server.close();

      oAuth2Client.getToken(code, (err, token) => {
        if (err) {
          return console.error(
            'Error while trying to retrieve access token',
            err,
          );
        }

        oAuth2Client.setCredentials(token);

        // Store the token to disk for later program executions
        fs.writeFile(config.googleToken, JSON.stringify(token), (err2) => {
          if (err2) {
            return console.error(err2);
          }

          console.log('Token stored to', config.googleToken);
        });

        callback(oAuth2Client);
      });
    } else {
      res.status(404).send(`Missing code!`);
    }
  });

  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: config.googleScopes,
  });

  console.log('Authorize this app by visiting this url:', authUrl);
}

const oAuth2Client = new google.auth.OAuth2(
  config.googleClientId,
  config.googleSecret,
  `http://localhost:${config.port}`,
);

fs.readFile(config.googleToken, (err, token) => {
  if (err) {
    return getNewToken(oAuth2Client, loggedIn);
  }

  oAuth2Client.setCredentials(JSON.parse(token.toString()));
  loggedIn(oAuth2Client);
});
