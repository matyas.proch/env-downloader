#!/usr/bin/env node

"use strict";
exports.__esModule = true;
var dotenv = require("dotenv");
var express = require("express");
var fs = require("fs");
var googleapis_1 = require("googleapis");
dotenv.config();
var config = {
    port: 4444,
    googleKey: process.env.ENV_DOWNLOADER_GOOGLE_KEY,
    googleClientId: process.env.ENV_DOWNLOADER_GOOGLE_CLIENT_ID,
    googleSecret: process.env.ENV_DOWNLOADER_GOOGLE_SECRET,
    googleToken: process.env.ENV_DOWNLOADER_GOOGLE_TOKEN_PATH,
    googleScopes: ['https://www.googleapis.com/auth/spreadsheets'],
    googleSpreadsheetId: process.env.ENV_DOWNLOADER_SPREADSHEET_ID,
    googleSheetName: process.env.ENV_DOWNLOADER_SHEET_NAME,
    outputEnv: process.env.ENV_DOWNLOADER_OUTPUT_PATH
};
var loggedIn = function (auth) {
    var sheets = googleapis_1.google.sheets({ version: 'v4', auth: auth });
    var download = function (data) {
        if (data.length === 0) {
            return;
        }
        var output = [];
        data.slice(1).forEach(function (row) {
            output.push(row[0] + "='" + row[1] + "'");
        });
        if (fs.existsSync(config.outputEnv)) {
            fs.unlinkSync(config.outputEnv);
        }
        fs.writeFileSync(config.outputEnv, output.join('\n'));
        console.log('Downloaded');
    };
    sheets.spreadsheets.values.get({
        key: config.googleKey,
        spreadsheetId: config.googleSpreadsheetId,
        range: config.googleSheetName
    }, function (err, res) {
        var _a;
        if (err) {
            return console.log("The API returned an error: " + err);
        }
        var data = (_a = res.data.values) !== null && _a !== void 0 ? _a : [];
        download(data);
    });
};
function getNewToken(oAuth2Client, callback) {
    var app = express();
    var server = app.listen(config.port, function () { });
    app.get('/', function (req, res) {
        var code = req.query.code;
        if (code) {
            res.status(200).send(code);
            server.close();
            oAuth2Client.getToken(code, function (err, token) {
                if (err) {
                    return console.error('Error while trying to retrieve access token', err);
                }
                oAuth2Client.setCredentials(token);
                // Store the token to disk for later program executions
                fs.writeFile(config.googleToken, JSON.stringify(token), function (err2) {
                    if (err2) {
                        return console.error(err2);
                    }
                    console.log('Token stored to', config.googleToken);
                });
                callback(oAuth2Client);
            });
        }
        else {
            res.status(404).send("Missing code!");
        }
    });
    var authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: config.googleScopes
    });
    console.log('Authorize this app by visiting this url:', authUrl);
}
var oAuth2Client = new googleapis_1.google.auth.OAuth2(config.googleClientId, config.googleSecret, "http://localhost:" + config.port);
fs.readFile(config.googleToken, function (err, token) {
    if (err) {
        return getNewToken(oAuth2Client, loggedIn);
    }
    oAuth2Client.setCredentials(JSON.parse(token.toString()));
    loggedIn(oAuth2Client);
});
